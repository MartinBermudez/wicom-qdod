<?php
/*

   Copyright 2017 GILIA

   Author: GILIA

   widocotest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("config.php", "config/");
load("widococonnector.php", "wicom/reasoner/");

use Wicom\Reasoner\WidocoConnector;

class WidocoConnectorTest extends PHPUnit\Framework\TestCase
{

    public function testWidocoConnector(){
        $input = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
    <Ontology xmlns="http://www.w3.org/2002/07/owl#"
          xml:base="http://localhost/kb1"
          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          xmlns:xml="http://www.w3.org/XML/1998/namespace"
          xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
          ontologyIRI="http://localhost/kb1">
        <Prefix name="" IRI="http://localhost/kb1#"/>
        <SubClassOf>
          <Class IRI="#Class1"/>
          <Class abbreviatedIRI="owl:Thing"/>
        </SubClassOf>
        <SubClassOf>
          <Class IRI="#Class3"/>
          <Class IRI="#Class1"/>
        </SubClassOf>
        <SubClassOf>
          <Class IRI="#Class2"/>
          <Class IRI="#Class1"/>
        </SubClassOf>
        <SubClassOf>
          <ObjectUnionOf>
            <Class IRI="#Class3"/>
            <Class IRI="#Class2"/>
          </ObjectUnionOf>
          <Class IRI="#Class1"/>
        </SubClassOf>
        <SubClassOf>
          <Class IRI="#Class1"/>
          <ObjectUnionOf>
            <Class IRI="#Class3"/>
            <Class IRI="#Class2"/>
          </ObjectUnionOf>
        </SubClassOf>
        <DisjointClasses>
          <Class IRI="#Class3"/>
          <Class IRI="#Class2"/>
        </DisjointClasses>
        <SubClassOf>
          <ObjectSomeValuesFrom>
            <ObjectProperty IRI="#R"/>
            <Class abbreviatedIRI="owl:Thing"/>
          </ObjectSomeValuesFrom>
          <Class IRI="#Class2"/>
        </SubClassOf>
        <SubClassOf>
          <ObjectSomeValuesFrom>
            <ObjectInverseOf>
              <ObjectProperty IRI="#R"/>
            </ObjectInverseOf>
            <Class abbreviatedIRI="owl:Thing"/>
          </ObjectSomeValuesFrom>
          <Class IRI="#Class4"/>
        </SubClassOf>
        <SubClassOf>
          <Class IRI="#Class2"/>
          <ObjectMinCardinality cardinality="1">
            <ObjectProperty IRI="#R"/>
          </ObjectMinCardinality>
        </SubClassOf>
        <SubClassOf>
          <Class IRI="#Class2"/>
          <ObjectMaxCardinality cardinality="5">
            <ObjectProperty IRI="#R"/>
          </ObjectMaxCardinality>
        </SubClassOf>
        <EquivalentClasses>
          <Class IRI="#Class2_R_min"/>
          <ObjectIntersectionOf>
            <Class IRI="#Class2"/>
            <ObjectMinCardinality cardinality="1">
              <ObjectProperty IRI="#R"/>
            </ObjectMinCardinality>
          </ObjectIntersectionOf>
        </EquivalentClasses>
        <EquivalentClasses>
          <Class IRI="#Class2_R_max"/>
          <ObjectIntersectionOf>
            <Class IRI="#Class2"/>
            <ObjectMaxCardinality cardinality="5">
              <ObjectProperty IRI="#R"/>
            </ObjectMaxCardinality>
          </ObjectIntersectionOf>
        </EquivalentClasses>
        <EquivalentClasses>
          <Class IRI="#Class4_R_min"/>
          <ObjectIntersectionOf>
            <Class IRI="#Class4"/>
            <ObjectMinCardinality cardinality="1">
              <ObjectInverseOf><ObjectProperty IRI="#R"/>
            </ObjectInverseOf>
          </ObjectMinCardinality>
        </ObjectIntersectionOf>
      </EquivalentClasses>
      <EquivalentClasses>
        <Class IRI="#Class4_R_max"/>
        <ObjectIntersectionOf>
          <Class IRI="#Class4"/>
          <ObjectMaxCardinality cardinality="1">
            <ObjectInverseOf>
              <ObjectProperty IRI="#R"/>
            </ObjectInverseOf>
          </ObjectMaxCardinality>
        </ObjectIntersectionOf>
      </EquivalentClasses>
      <AnnotationAssertion>
        <AnnotationProperty IRI="#X"/>
        <IRI>#Class1</IRI>
        <Literal datatypeIRI="http://www.w3.org/2001/XMLSchema#float">43.0</Literal>
    </AnnotationAssertion>
    <AnnotationAssertion>
        <AnnotationProperty IRI="#Y"/>
        <IRI>#Class1</IRI>
        <Literal datatypeIRI="http://www.w3.org/2001/XMLSchema#float">6.0</Literal>
    </AnnotationAssertion>
    <AnnotationAssertion>
        <AnnotationProperty IRI="#ot_name_ann"/>
        <IRI>#Class1</IRI>
        <IRI>#Class1</IRI>
    </AnnotationAssertion>
    <AnnotationAssertion>
        <AnnotationProperty IRI="#X"/>
        <IRI>#R</IRI>
        <Literal datatypeIRI="http://www.w3.org/2001/XMLSchema#float">3.0</Literal>
    </AnnotationAssertion>
    <AnnotationAssertion>
        <AnnotationProperty IRI="#Y"/>
        <IRI>#R</IRI>
        <Literal datatypeIRI="http://www.w3.org/2001/XMLSchema#float">14.0</Literal>
    </AnnotationAssertion>
    <AnnotationAssertion>
        <AnnotationProperty IRI="#rel_name_ann"/>
        <IRI>#R</IRI>
        <IRI>#R</IRI>
    </AnnotationAssertion>
    </Ontology>
XML;

        $widoco = new WidocoConnector();

        //$GLOBALS['config']['temporal_path'] = "../../temp";

        $widoco->run($input);

    }
}
