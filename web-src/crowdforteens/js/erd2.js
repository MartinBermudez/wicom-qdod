//graph para el diagrama er
var graphMain = new joint.dia.Graph();
//paper para manejar el graph diagrama er
var paper = new joint.dia.Paper({
		el: document.getElementById('paper'),
		width: $('#paper').width(),
		height: $('#paper').height(),
		model: graphMain,
		gridSize: 10,
		defaultLink: function () {
			return createLink();
		},
		connectionStrategy: function (end) {
			// removing `magnet: 'tool'` from the end object
			return {
				id: end.id
			};
		},
		validateConnection: function (cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
			// previene conexiones invalidas
			/*if (cellViewS != null && cellViewT != null) {
			console.log("source: " + cellViewS.model.attributes.type + ", target: " + cellViewT.model.attributes.type);
			}*/
			if (cellViewS == null || cellViewT == null || cellViewS.model.attributes.type === 'standard.Link' || cellViewT.model.attributes.type === 'standard.Link') {
				//console.log("no conecta");
				return false;
			}
			return canConnect(cellViewS.model, cellViewT.model);
		},
		//interactive: { labelMove: true },
		/*snapLinks: {
		radius: 75
		},*/
		//linkPinning: false,
		//fitToContent: true,
		drawGrid: {
			name: 'doubleMesh',
			args: [{
					color: '#4C678C',
					thickness: 1
				}, // settings for the primary mesh
				{
					color: '#4C678C',
					scaleFactor: 10,
					thickness: 5
				} //settings for the secondary mesh
			]
		}
	});

function getSpecificType(elementModel) {
	var type = elementModel.attributes.type;
	var resultType;
	if (type === 'erd.Entity' || type === 'erd.CustomEntity') {
		resultType = 'Entity';
    } else if (type === 'erd.WeakEntity') {
        resultType = 'WeakEntity';
	} else if (type === 'erd.Relationship') {
		resultType = 'Relationship';
    } else if (type === 'erd.IdentifyingRelationship') {
        resultType = 'WeakRelationship';
	} else if (type === 'erd.Normal') {
		resultType = 'Attribute';
    } else if (type === 'erd.Key') {
        resultType = 'KeyAttribute';
	} else if (type === 'erd.Multivalued') {
        resultType = 'MultivaluedAttribute';
    } else if (type === 'erd.WeakKey') {
        resultType = 'WeakKeyAttribute';
    } else if (type === 'erd.Derived') {
        resultType = 'DerivedAttribute';
    } else if (type === 'erd.ISA') {
		resultType = 'Inheritance';
	} else {
		resultType = 'Error';
	}
	return resultType;
}

function getType(elementModel) {
	var type = elementModel.attributes.type;
	var resultType;
	if (type === 'erd.Entity' || type === 'erd.CustomEntity' || type === 'erd.WeakEntity') {
		resultType = 'Entity';
	} else if (type === 'erd.Relationship' || type === 'erd.IdentifyingRelationship') {
		resultType = 'Relationship';
	} else if (type === 'erd.Normal' || type === 'erd.Key' || type === 'erd.Multivalued' || type === 'erd.Derived' || type === 'erd.WeakKey') {
		resultType = 'Attribute';
	} else if (type === 'erd.ISA') {
		resultType = 'Inheritance';
	} else {
		resultType = 'Error';
	}
	return resultType;
}

function canConnect(cellViewS, cellViewT) {
	var canConnect = true;
	if (cellViewS != null && cellViewT != null) {
		var sourceType = getType(cellViewS);
		var targetType = getType(cellViewT);
		//var linksS = getElementLinks(cellViewS);
		//var linksT = getElementLinks(cellViewT);
		//console.log(linksS);
		//console.log(linksT);
		if (cellViewS === cellViewT) {
			canConnect = false;
		} else if (sourceType === 'Entity') {
			if (targetType === 'Entity') {
				canConnect = false;
			}
		} else if (sourceType === 'Relationship') {
			if (targetType === 'Relationship' || targetType === 'Inheritance') {
				canConnect = false;
			}
		} else if (sourceType === 'Attribute') {
			if (targetType === 'Inheritance') {
				canConnect = false;
			}
		} else if (sourceType === 'Inheritance') {
			if (targetType === 'Relationship' || targetType === 'Attribute' || targetType === 'Inheritance') {
				canConnect = false;
			}
		}
	} else {
		canConnect = false;
	}
	return canConnect;
}

function getElementLinks(element) {
	return graphMain.getConnectedLinks(element);
}

function getAllElement() {
	//retorna todos los elementos en un arreglo con la forma [entidades,relaciones,atributos,herencias,conectores]
	var entities = [];
    var relationships = [];
    var attributes = [];
    var inheritances = [];
    var connectors = [];
    var elements = graphMain.getElements();
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var type = getSpecificType(element);
        var name = element.attr('text/text');
        var cid = element.cid;
        var id = cid.match(/\d+/g)[0];
        switch (type) {
            case "Entity": 
                var entity = {"name":name,"id":id,"weak":false};
                entities.push(entity);
                break;
            case "WeakEntity": 
                var entity = {"name":name,"id":id,"weak":true};
                entities.push(entity);
                break;
            case "Relationship": 
                var relationship = {"name":name,"id":id,"weak":false};
                relationships.push(relationship);
                break;
            case "WeakRelationship":
                var relationship = {"name":name,"id":id,"weak":true};
                relationships.push(relationship);
                break;
            case "Attribute": 
                var attribute = {"weakKey": false,"dataType": "int","multivalued": false,"name": name,"id": id,"derived": false,"key": false,"multivaluedValue": ""};
                attributes.push(attribute);
                break;
            case "KeyAttribute":
                var attribute = {"weakKey": false,"dataType": "int","multivalued": false,"name": name,"id": id,"derived": false,"key": true,"multivaluedValue": ""};
                attributes.push(attribute);
                break;
            case "MultivaluedAttribute": 
                var attribute = {"weakKey": false,"dataType": "int","multivalued": true,"name": name,"id": id,"derived": false,"key": false,"multivaluedValue": "value"};
                attributes.push(attribute);
                break;
            case "WeakKeyAttribute": 
                var attribute = {"weakKey": true,"dataType": "int","multivalued": false,"name": name,"id": id,"derived": false,"key": false,"multivaluedValue": ""};
                attributes.push(attribute);
                break;
            case "DerivedAttribute":
                var attribute = {"weakKey": false,"dataType": "int","multivalued": false,"name": name,"id": id,"derived": true,"key": false,"multivaluedValue": ""};
                attributes.push(attribute);
                break;
            case "Inheritance":
                var inheritance = {"id":id,"type":0};
                inheritances.push(inheritance);
                break;
        }
    }
	return [entities,relationships,attributes,inheritances,connectors];
}

//graph para la paleta er
var paletteGraph = new joint.dia.Graph();
//graph para manejar la paleta
var palette = new joint.dia.Paper({
		el: document.getElementById('palette'),
		width: $('#palette').width(),
		height: $('#palette').height(),
		model: paletteGraph,
		interactive: false,
		//gridSize: 100,
		/*drawGrid:
	{
		name: 'mesh',
		args: [
	{
		color: 'white',
		thickness: 1
		}
		]
		}*/
	});

// var paperSmall = new joint.dia.Paper({
//         el: document.getElementById('minimap'),
//         model: graphMain,
//         width: $('#minimap').width(),
//         height: $('#minimap').height(),
//         gridSize: 1,
//         interactive: false
//     });
// paperSmall.scale(0.25);

//para el drag and drop de la paleta
palette.on('cell:pointerdown', function (cellView, e, x, y) {
	$('body').append('<div id="flyPaper"></div>');
	var flyGraph = new joint.dia.Graph,
	flyPaper = new joint.dia.Paper({
			el: $('#flyPaper'),
			model: flyGraph,
			interactive: false,
		}),
	flyShape = cellView.model.clone(),
	pos = cellView.model.position(),
	offset = {
		x: flyShape.attributes.size.width / 2 * paper.scale().sx,
		y: flyShape.attributes.size.height / 2 * paper.scale().sy
	};
	flyPaper.scale(paper.scale().sx);
	flyShape.position(0, 0);
	flyGraph.addCell(flyShape);
	$("#flyPaper").offset({
		left: (e.pageX - offset.x),
		top: (e.pageY - offset.y)
	});
	$('body').on('mousemove.fly', function (e) {
		$("#flyPaper").offset({
			left: (e.pageX - offset.x),
			top: (e.pageY - offset.y)
		});
	});
	$('body').on('mouseup.fly', function (e) {
		var x = e.pageX,
		y = e.pageY,
		target = paper.$el.offset();
		origin = palette.$el.offset();

		// Dropped over paper and not over origin
		if ((x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) &&
			!(x > origin.left && x < origin.left + palette.$el.width() && y > origin.top && y < origin.top + palette.$el.height())) {
			var s = flyShape.clone();
			var p = paper.clientToLocalPoint(e.clientX, e.clientY);
			/*var localRect1 = paper.clientToLocalRect(target.left,target.top,target.width,target.height);
			s.position(((x - target.left - offset.x)+localRect1.center().x), ((y - target.top - offset.y)+localRect1.center().y));*/
			s.position(p.x - (s.attributes.size.width / 2), p.y - (s.attributes.size.height / 2));
			graphMain.addCell(s);
		}
		$('body').off('mousemove.fly').off('mouseup.fly');
		flyShape.remove();
		$('#flyPaper').remove();
	});
});

//para mover la paleta
var dragStartPositionPalette;

palette.on('blank:pointerdown', function (event, x, y) {
	if (!fixedPalette) {
		dragStartPositionPalette = {
			x: x,
			y: y
		};

		$('body').on('mousemove.fly', function (event) {
			if (dragStartPositionPalette != null) {
				$("#palette").offset({
					left: event.pageX - dragStartPositionPalette.x, //$("#palette").width() / 2,
					top: event.pageY - dragStartPositionPalette.y //$("#palette").height() / 2
				});
			}
		});

		$('body').on('mouseup.fly', function (e) {
			dragStartPositionPalette = null;
			$('body').off('mousemove.fly').off('mouseup.fly');
		});
	}
});

// herramientas para la paleta
var fixedPalette = true;
var opacityPalette = false;
var extendedPalette = false;
var horizontalPalette = false;

var paletteTools = $('<div id="paletteTools" class="toolbar-palette">');
paletteTools.append('<div id="paletteFixButton" class="tools tools-palette-fix" onclick="paletteFix()"><i class="material-icons">lock</i></div>');
paletteTools.append('<div id="paletteResetButton" class="tools tools-palette-reset" onclick="paletteReset()"><i class="material-icons">picture_in_picture_alt</i></div>');
paletteTools.append('<div id="paletteOpacityButton" class="tools tools-palette-opacity" onclick="paletteOpacity()"><i class="material-icons">visibility</i></div>');
paletteTools.append('<div id="paletteExtendButton" class="tools tools-palette-extend" onclick="paletteExtend()"><i class="material-icons">remove_circle</i></div>');
//paletteTools.append('<div id="paletteRotateButton" class="tools tools-palette-rotate" onclick="paletteRotate()"><i class="material-icons">rotate_90_degrees_ccw</i></div>');
paletteTools.css('display', 'none');

$('#palette').append(paletteTools);

$("#palette").mouseover(function () {
	paletteTools.css('display', 'block');
});

$("#palette").mouseleave(function () {
	paletteTools.css('display', 'none');
});

function paletteFix() {
	fixedPalette = !fixedPalette;
	if (fixedPalette) {
		document.getElementById("paletteFixButton").innerHTML = '<i class="material-icons">lock</i>';
	} else {
		document.getElementById("paletteFixButton").innerHTML = '<i class="material-icons">lock_open</i>';
	}
}

function paletteReset() {
	var lastOpacity = document.getElementById("palette").style.opacity;
	var lastHeight = document.getElementById("palette").style.height;
	var lastWidth = document.getElementById("palette").style.width;
	document.getElementById("palette").style = null;
	document.getElementById("palette").style.opacity = lastOpacity;
	document.getElementById("palette").style.height = lastHeight;
	document.getElementById("palette").style.width = lastWidth;
}

function paletteOpacity() {
	opacityPalette = !opacityPalette;
	if (opacityPalette) {
		document.getElementById("paletteOpacityButton").innerHTML = '<i class="material-icons">visibility_off</i>';
		document.getElementById("palette").style.opacity = ".7";
	} else {
		document.getElementById("paletteOpacityButton").innerHTML = '<i class="material-icons">visibility</i>';
		document.getElementById("palette").style.opacity = "1";
	}
}

var paletteElements = paletteElements();
var paletteElementsReduced = paletteElementsReduced();
var actualElements = paletteElementsReduced;

sortPalette(false, 5, paletteElements);

paletteGraph.addCells(actualElements);

function paletteExtend() {
	extendedPalette = !extendedPalette;
	if (extendedPalette) {
		document.getElementById("paletteExtendButton").innerHTML = '<i class="material-icons">add_circle</i>';
		actualElements = paletteElements;
		if (horizontalPalette) {
			document.getElementById("palette").style.height = "201px";
		} else {
			document.getElementById("palette").style.width = "201px";
		}
	} else {
		document.getElementById("paletteExtendButton").innerHTML = '<i class="material-icons">remove_circle</i>';
		actualElements = paletteElementsReduced;
		if (horizontalPalette) {
			document.getElementById("palette").style.height = "101px";
		} else {
			document.getElementById("palette").style.width = "101px";
		}
	}
	paletteGraph.clear();
	paletteGraph.addCells(actualElements);
}

function paletteRotate() {
	horizontalPalette = !horizontalPalette;
	sortPalette(horizontalPalette, 5, paletteElements);
	sortPalette(horizontalPalette, 5, paletteElementsReduced);
	paletteGraph.clear();
	paletteGraph.addCells(actualElements);
	/*if (horizontalPalette) {
	document.getElementById("palette").className = "palette-horizontal";
	} else {
	document.getElementById("palette").className = "palette-vertical";
	}
	document.getElementById("palette").style = null;
	document.getElementById("paletteTools").style = null;*/
	var actualWidth = document.getElementById("palette").style.width;
	var actualHeight = document.getElementById("palette").style.height;
	document.getElementById("palette").style.width = actualHeight;
	document.getElementById("palette").style.height = actualWidth;
}

/*para herramientas de edicion de elemento*/
/*paper.on('element:delete', function (elementView, evt) {
// Stop any further actions with the element view e.g. dragging
evt.stopPropagation();
if (confirm('Are you sure you want to delete this element?')) {
elementView.model.remove();
}
});*/

var actualElement = null;
var actualRenameElement = null;

var tools = $('<div class="toolbar">');
tools.append('<div id="elementDeleteButton" class="tools tools-delete" onclick="elementDelete()"><i class="material-icons">delete_forever</i></div>');
//tools.append('<div class="tools tools-clearlink">C</div>');
//tools.append('<div class="tools tools-newnext">N</div>');
tools.append('<div id="elementAddAttr" class="tools tools-add-attr" onclick="addAttr(event)"><i class="material-icons">vignette</i></div>');
tools.append('<div id="elementLinkButton" class="tools tools-link" onmousedown="elementLink(event)"><i class="material-icons">trending_up</i></div>');
tools.append('<div id="elementDuplicateButton" class="tools tools-duplicate" onclick="elementDuplicate()""><i class="material-icons">file_copy</i></div>');
tools.append('<div id="elementNameText" class="tools tools-rename"><textarea id="elementRenameInput" cols="5" rows="2"></textarea></div>');
tools.append('<div id="elementAttrType" class="tools tools-attr-type"><i class="material-icons" onclick="displayAttrType()">title</i><select id="selectAttrType" size="3"><option value="varchar">Texto</option><option value="integer">Entero</option><option value="boolean">Booleano</option></select></div>');
tools.css({
	display: 'none'
});

$('#paper').append(tools);

disableTool('#elementLinkButton');
disableTool('#elementDuplicateButton');

function disableTool(tool) {
	$(tool).css({
		'background': '#787575',
		'pointer-events': 'none',
	});
}

paper.on('element:pointerclick', function (cellView, evt) {
	actualElement = cellView;
	showElementTools(cellView.model);
	hideRenameText();
});

paper.on('element:pointerdblclick', function (cellView, evt) {
	if (getType(cellView.model) != 'Inheritance') {
		$('#elementNameText').css({
			display: 'block'
		});
		$('#elementRenameInput').val(cellView.model.attr('text/text'));
		$('#elementRenameInput').focus();
		actualRenameElement = cellView;
	}
});

$('textarea').on('keydown', function (e) {
	if (e.which == 13 && !e.shiftKey) {
		hideRenameText();
	}
});

$('body').on('keydown', function (e) {
	if (e.which == 27) {
		actualRenameElement = null;
		hideRenameText();
	}
});

$('body').on('keydown', function (e) {
	if (e.key === "Delete") {
		if (actualElement != null) {
			elementDelete();
		}
	}
});

paper.on('element:pointermove', function (cellView, evt) {
	hideElementTools();
});

paper.on('blank:pointerdown', function (cellView, evt) {
	hideElementTools();
});

$('#paper').on('mousewheel DOMMouseScroll', function (evt) {
	hideElementTools();
});

$('#elementNameText').focusout(function () {
	hideElementTools();
});

function showElementTools(figure) {
	var pos = paper.localToClientPoint(figure.attributes.position);
	//var screenPos = paper.localToClientPoint(pos);

	tools.width(figure.attributes.size.width * paper.scale().sx + $('.tools').width() * 2);
	tools.height(figure.attributes.size.height * paper.scale().sy + $('.tools').height() * 2);
	tools.attr('elementid', figure.id);
	tools.css({
		top: pos.y - $('.tools').height(),
		left: pos.x - $('.tools').width(),
		display: 'block'
	});

	var displayAttrType = (getType(actualElement.model) == 'Attribute') ? 'block' : 'none';
	//oculta la opcion de cambiar el tipo del atributo si no es un atributo
	$('#elementAttrType').css({
		display: displayAttrType
	});
	//oculta el select de tipo del atributo
	$('#selectAttrType').css({
		display: 'none'
	});
	var displayAddAttr = (getType(actualElement.model) == 'Entity' || getType(actualElement.model) == 'Relationship') ? 'block' : 'none';
	//oculta/muestra la opcion de agregar un atributo
	$('#elementAddAttr').css({
		display: displayAddAttr
	});
}

function hideElementTools() {
	tools.css('display', 'none');
	actualElement = null;
	hideRenameText();
}

function hideRenameText() {
	$('#elementNameText').css({
		display: 'none'
	});
	if (actualRenameElement != null) {
		changeName(actualRenameElement, $('#elementRenameInput').val());
	}
	$('#elementRenameInput').val("");
}

function elementDelete() {
	var cell = graphMain.getCell(tools.attr('elementid'));
	cell.remove();
	//actualElement.remove();
	hideElementTools();
}

function elementLink(event) {
	var newLink = createLink();
	newLink.source({
		id: actualElement.model.id
	});
	newLink.prop('target', {
		x: event.clientX,
		y: event.clientY
	});
	//newLink.startArrowheadMove('target');
	hideElementTools();

	//graphMain.startBatch("arrowhead-move",newLink);
	//graphMain.dragArrowheadStart(evt, x, y);
	//console.log(newLink);
	//newLink.dragArrowheadStart();
	/*$('#paper').on('mousemove.fly', function (event) {
	//console.log(newLink);
	newLink.prop('target', {
	x: event.pageX,
	y: event.pageY
	});
	});

	$('#paper').on('mouseup.fly', function (event) {
	$('#paper').off('mousemove.fly').off('mouseup.fly');
	});*/
}

function elementDuplicate() {
	hideElementTools();
}

function displayAttrType() {
	$('#selectAttrType').css({
		display: 'block'
	});
	//selecciona por defecto el tipo de dato que ya es el atributo
	$('#selectAttrType').val(actualElement.model.attr('customAttr/type'));
}

$('#selectAttrType').click(function () {
	actualElement.model.attr('customAttr/type',$(this).val());
	hideElementTools();
})

function changeName(elementView, value) {
	if (value.length > 0) {
		elementView.model.attr('text/text', value);
	}
}

function addAttr(event){
	//crear atributo
	var newAttr = paletteCreateAttr();
	graphMain.addCell(newAttr);
	//var newAttr = tempAttr.clone();
	newAttr.position(actualElement.model.position().x,actualElement.model.position().y-80);
	//crear link
	var link = createLink();
	//conectar link a attr y actualElement
	connectLink(link, actualElement.model, newAttr);
	hideElementTools();
}

/*graphMain.on('all', function(eventName, cell) {
console.log(arguments);
});*/

//exportar json para omelet
function exportJSON() {
	for (var i = 0; i < paper.model.getElements().length; i++) {
		console.log(paper.model.getElements()[i].attr('text/text') + ' ');
	}
}

//crea un conector
var createLink = function () {
	//para agregar controles en conectores
	var verticesTool = new joint.linkTools.Vertices();
	var segmentsTool = new joint.linkTools.Segments();
	//var sourceArrowheadTool = new joint.linkTools.SourceArrowhead();
	//var targetArrowheadTool = new joint.linkTools.TargetArrowhead();
	var sourceAnchorTool = new joint.linkTools.SourceAnchor();
	var targetAnchorTool = new joint.linkTools.TargetAnchor();
	var boundaryTool = new joint.linkTools.Boundary();
	var removeButton = new joint.linkTools.Remove({
			distance: 20
		});

	var toolsView = new joint.dia.ToolsView({
			tools: [
				verticesTool, segmentsTool,
				//sourceArrowheadTool, targetArrowheadTool,
				sourceAnchorTool, targetAnchorTool,
				boundaryTool, removeButton
			]
		});

	var myLink = new joint.shapes.standard.Link();

	myLink.attr({
		line: {
			stroke: 'black',
			strokeWidth: 2,
			sourceMarker: {},
			targetMarker: {
				'd': ''
			}
		},
		customAttr: {
			total: false
		}
	});

	myLink.connector('jumpover', {
		size: 10
	});

	var link = myLink.addTo(graphMain);
	var linkView = myLink.findView(paper);
	linkView.addTools(toolsView);

	return link;
};

paper.on('link:mouseenter', function (actualLinkView) {
	//console.log(actualLinkView);
	actualLinkView.showTools();
});

paper.on('blank:mouseover', function (actualLinkView) {
	paper.hideTools();
});

var connectLink = function (myLink, elm1, elm2) {
	myLink.source({
		id: elm1.id
	});
	myLink.target({
		id: elm2.id
	});
};

//devuelve los labels para agregar a un conector
var createLabel = function (txt) {
	var label = {
		attrs: {
			text: {
				text: txt,
				fill: 'black',
				fontSize: 20
			},
			rect: {
				fill: 'white'
			}
		},
		position: {
			distance: 0.5,
			offset: 15
		}
	}
	return label;
};

var dataElement;

// para intercambiar la cardinalidad de un link
paper.on('link:pointerdblclick', function (linkView, evt) {
	//console.log(linkView);
	var labels = linkView.model.attributes.labels;
	if (labels.length > 0) {
		//console.log(labels[0].attrs.text.text);
		if (labels[0].attrs.text.text == '1') {
			//labels[0].attrs.text.text = 'N';
			linkView.model.label(0, {
				attrs: {
					text: {
						text: 'N'
					}
				}
			});
		} else {
			//labels[0].attrs.text.text = '1';
			linkView.model.label(0, {
				attrs: {
					text: {
						text: '1'
					}
				}
			});
		}
	}
});

//para cambiar el tipo de conector a conector total
paper.on('link:contextmenu', function (linkView, evt) {
	/*console.log(linkView.model.source().id);
	console.log(graphMain.getCell(linkView.model.source().id));
	console.log(graphMain.getCell(linkView.model.target().id));*/
	//conectores totales solo para conexiones entre elementos que no sean atributos
	if (getType(graphMain.getCell(linkView.model.source().id)) != 'Attribute' && 
		getType(graphMain.getCell(linkView.model.target().id)) != 'Attribute'){
	if (!linkView.model.attr('customAttr').total){
	//la hace total
	linkView.model.attr({
			line: {
				strokeWidth: 5
				//strokeDasharray: '15 5',
            	//strokeDashoffset: 15,
        },
        customAttr : {
        	total: true
        }
    });
}else{
		linkView.model.attr({
			line: {
				strokeWidth: 2,
				strokeDasharray: 'none',
            	strokeDashoffset: 'none',
        },
        customAttr : {
        	total: false
        }
    });
}
}
});

//para cambiar una entidad a superclase
paper.on('element:contextmenu', function (elementView, evt) {
	//console.log("neig "+graphMain.getNeighbors(elementView.model).length);
	if (getType(elementView.model)=="Entity"){
	var arr = graphMain.getNeighbors(elementView.model);
	for (var i = 0; i< arr.length; i++) {
		console.log(arr[i].attr("text/text"));
		if (arr[i].attr("text/text")=='ISA'){
			if (!elementView.model.attr('customAttr').super){
				elementView.model.attr({
					customAttr : {
		        	super: true
		        	},
		        	'.outer': {
					fill: 'black'
					}
	    		});
			}else{
				elementView.model.attr({
					customAttr : {
		        	super: false
		        	},
		        	'.outer': {
					fill: '#083c5d'
					}
	    		});
			}
		}
	}
	}
});

// para crear links arrastrando una elemento sobre otro
paper.on({
	'element:pointerdown': function (elementView, evt) {
		evt.data = elementView.model.position();
		dataElement = evt;
	},

	'element:pointerup': function (elementView, evt, x, y) {
		var coordinates = new g.Point(x, y);
		var elementAbove = elementView.model;
		var elementBelow = this.model.findModelsFromPoint(coordinates).find(function (el) {
				return (el.id !== elementAbove.id);
			});

		// If the two elements are connected already, don't
		// connect them again (this is application-specific though).
		if (elementBelow && graphMain.getNeighbors(elementBelow).indexOf(elementAbove) === -1 && canConnect(elementBelow, elementAbove)) {

			// Move the element to the position before dragging.
			elementAbove.position(dataElement.data.x, dataElement.data.y);

			// Create a connection between elements.
			// var link = new joint.shapes.standard.Link();
			// link.source(elementAbove);
			// link.target(elementBelow);
			// link.addTo(graphMain);
			var newLink = createLink();
			connectLink(newLink, elementBelow, elementAbove);
			if (getType(elementBelow) != 'Attribute' && getType(elementAbove) != 'Attribute' &&
				getType(elementBelow) != 'Inheritance' && getType(elementAbove) != 'Inheritance') {
				newLink.appendLabel(createLabel('1'));
			}

			// Add remove button to the link.
			// var tools = new joint.dsia.ToolsView({
			//     tools: [new joint.linkTools.Remove()]
			// });
			// link.findView(this).addTools(tools);
		}
	}
});

//para dragear la pagina

var dragStartPositionMain;

paper.on('blank:pointerdown', function (event, x, y) {
	//dragStartPositionMain = { x: x, y: y};
	var scale = paper.scale();
	dragStartPositionMain = {
		x: x * scale.sx,
		y: y * scale.sy
	};
});

$("#paper").mousemove(function (event) {
	if (dragStartPositionMain != null) {
		//console.log("mousemove");
		paper.translate(
			event.offsetX - dragStartPositionMain.x,
			event.offsetY - dragStartPositionMain.y);
	}
});

paper.on('cell:pointerup blank:pointerup', function (cellView, x, y) {
	dragStartPositionMain = null;
});

// var dragStartPositionPalette;

// palette.on('blank:pointerdown',function(event, x, y) {
//         dragStartPositionPalette = { x: x, y: y};
//     }
// );

// $("#palette").mousemove(function(event) {
//     if (dragStartPositionPalette != null) {
//         console.log("mousemove");
//         palette.translate(
//             event.offsetX - dragStartPositionPalette.x,
//             event.offsetY - dragStartPositionPalette.y);
//     }
// });

// palette.on('cell:pointerup blank:pointerup', function(cellView, x, y) {
//     dragStartPositionPalette = null;
// });

// $("#paper").on('mousewheel', function(event) {
//     console.log(event);
//     console.log("wheeeee");
//     var oldScale = paper.scale().sx;
//     var newScale = oldScale + event.deltaY/10;
//     var beta = oldScale/newScale;

//     var mouseLocal = paper.paperToLocalPoint(event.clientX, event.clientY);
//     console.log(mouseLocal);
//     var p = {x:mouseLocal.x, y:mouseLocal.y};

//     ax = p.x - (p.x * beta) ;
//     ay = p.y - (p.y * beta) ;

//     paper.scale(newScale, newScale, ax, ay);
// })

$('#paper').on('mousewheel DOMMouseScroll', function (evt) {

	evt.preventDefault();
	var p = paper.clientToLocalPoint({
			x: evt.clientX,
			y: evt.clientY
		});
	var delta = Math.max(-1, Math.min(1, (evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
	var currentScale = V(paper.viewport).scale().sx;
	var newScale = currentScale + delta / 50;
	if (newScale > 0.4 && newScale < 2) {
		//paper.translate(0, 0);
		paper.scale(newScale, newScale); //, p.x, p.y);
	}
});

/*para que los div se acomoden al tamaño de la ventana*/
$(window).resize(function () {
	//$("#palette").width($(window).width()*0.25);
	//$("#palette").height($(window).height());
	$("#paper").width($(window).width());
	$("#paper").height($(window).height());
});

/*para editar un elemento*/
/*paper.on({
'element:pointerdblclick': function (elementView, evt) {
//elementView.model.attr('text/text',"aaaaaaaa");
//elementView.model.attr('text/text',$("_"+elementView.model.id).val());

//$('body').append('<div class="editElement"><input id="_'+elementView.model.id+'" name="nameEntity" type="text"><button type="button" onclick="'+json_encode(elementView.model)+';">Aceptar</button><button type="button" onclick="$(\'.editElement\').remove();">Cancelar</button> </div>');
//append('<button type="button">Aceptar</button>').click(changeName(elementView)).append('<button type="button" onclick="$(\'.editElement\').remove();">Cancelar</button>');
$('body').append('<div id="flyConfig' + elementView.model.id + '" class="flyConfig"></div>');
$('#flyConfig' + elementView.model.id).width(elementView.model.attributes.size.width);
$('#flyConfig' + elementView.model.id).height(elementView.model.attributes.size.height);
//console.log(elementView.model.attributes.size.width + " x " + elementView.model.attributes.size.height);
console.log(elementView.model);
$("#flyConfig" + elementView.model.id).offset({
left: elementView.model.attributes.position.x,
top: elementView.model.attributes.position.y
});
//var html = '<div class="div' + elementView.model.id + '" style="z-index:100; position:fixed"><input id="txtNewName" class="txt' + elementView.model.id + '" name="nameEntity" type="text"></div>';
//$("body").append(html)
//var btnAccept = '<button class="btn' + elementView.model.id + '" type="button">Aceptar</button>';
//$(".div" + elementView.model.id).append(btnAccept);
//$(".btn" + elementView.model.id).click(function ()
//{
//changeName(elementView)
//}
//);
//var btnCancel = '<button type="button" onclick="$(\'.div' + elementView.model.id + '\').remove();">Cancelar</button>';
//$(".div" + elementView.model.id).append(btnCancel);
}
});*/

// function clientToLocalPoint(p) {

//       var svgPoint = paper.svg.createSVGPoint();
//       svgPoint.x = p.x;
//       svgPoint.y = p.y;

//     // This is a hack for Firefox! If there wasn't a fake (non-visible) rectangle covering the
//     // whole SVG area, `$(paper.svg).offset()` used below won't work.
//     var fakeRect = V('rect', {
//         width: paper.options.width,
//         height: paper.options.height,
//         x: 0,
//         y: 0,
//         opacity: 0
//     });
//     V(paper.svg).prepend(fakeRect);

//     var paperOffset = $(paper.svg).offset();

//     // Clean up the fake rectangle once we have the offset of the SVG document.
//     fakeRect.remove();

//     var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
//     var scrollLeft = document.body.scrollLeft || document.documentElement.scrollLeft;

//     svgPoint.x += scrollLeft - paperOffset.left;
//     svgPoint.y += scrollTop - paperOffset.top;

//     // Transform point into the viewport coordinate system.
//     var pointTransformed = svgPoint.matrixTransform(paper.viewport.getCTM().inverse());

//     return pointTransformed;
// }

function verificarSintaxis() {
	var elements = graphMain.getCells();
	for (var i = 0; i < elements.length; i++) {
		element = elements[i];
		var links = getElementLinks(element);
		if (links.length < 1) {
			//el elemento no tiene ningún link
			markElement(element);
		}
		if (getType(element) == 'Entity') {
			//la entidad tiene que tener al menos un atributo clave
			checkEntity(element, links);
		}
		if (getType(element) == 'Attribute') {
			//solo puede estar conectado a 1 entidad y/o relacion
			checkAttribute(element, links);
		}
		if (getType(element) == 'Relationship') {
			//debe tener por lo menos 2 entidades conectadas
			checkRelationship(element, links);
		}
		if (getType(element) == 'Inheritance') {
			//debe tener por lo menos 2 entidades conectadas
			//y una superclase
			checkInheritance(element, links);
		}
	}
}

function checkEntity(element, links) {
	//verifica que entre los links haya un atributo clave
	for (var i = 0; i < links.length; i++) {
		link = links[i];
		var elm = graphMain.getCell(link.source().id);
		if (getSpecificType(elm) == 'KeyAttribute') {
			return;
		}
		var elm2 = graphMain.getCell(link.target().id);
		if (getSpecificType(elm2) == 'KeyAttribute') {
			return;
		}
	}
	markElement(element);
}

function checkAttribute(element, links) {
	//verifica que solo haya 1 entidad o relacion
	var cant = 0;
	for (var i = 0; i < links.length; i++) {
		link = links[i];
		var elm = graphMain.getCell(link.source().id);
		if (getType(elm) == 'Entity' || getType(elm) == 'Relationship') {
			cant++;
		}
		var elm2 = graphMain.getCell(link.target().id);
		if (getType(elm2) == 'Entity' || getType(elm2) == 'Relationship') {
			cant++;
		}
	}
	if (cant > 1) {
		markElement(element);
	}
}

function checkRelationship(element, links) {
	//verifica que haya al menos 2 entidades
	var cant = 0;
	for (var i = 0; i < links.length; i++) {
		link = links[i];
		var elm = graphMain.getCell(link.source().id);
		if (getType(elm) == 'Entity') {
			cant++;
		}
		var elm2 = graphMain.getCell(link.target().id);
		if (getType(elm2) == 'Entity') {
			cant++;
		}
	}
	if (cant < 2) {
		markElement(element);
	}
}

function checkInheritance(element, links) {
	//debe tener por lo menos 2 entidades conectadas
	//y una superclase
	var cant = 0;
	var notExistSuper = true;
	for (var i = 0; i < links.length; i++) {
		link = links[i];
		var elm = graphMain.getCell(link.source().id);
		if (getType(elm) == 'Entity') {
			cant++;
			if (elm.attr('customAttr').super) {
				if (notExistSuper) {
					notExistSuper = false;
				} else {
					//ya existe otra entidad como super
					markElement(element);
					//return;
				}
			}
		}
		var elm2 = graphMain.getCell(link.target().id);
		if (getType(elm2) == 'Entity') {
			cant++;
			if (elm2.attr('customAttr').super) {
				if (notExistSuper) {
					notExistSuper = false;
				} else {
					//ya existe otra entidad como super
					markElement(element);
					//return;
				}
			}
		}
	}
	if (cant < 2 || notExistSuper) {
		markElement(element);
	}
}

function inferir() {
	cleanInference();
	verificarSintaxis();
	var req = consultarOmelet();
	var rr;
	// Gestor del evento que indica el final de la petición (la respuesta se ha recibido)
	req.addEventListener("load", function() {
	// La petición ha tenido éxito
	if (req.status >= 200 && req.status < 400) {
	    console.log(req.responseText);
	    rr = req.responseText.toString();
		if (rr=='[]'){
			alert('Modelo consistente');
		}else{
			//quitar corchetes
			var r2 = rr.substring(1,rr.length-1);
			var classes = r2.split(',');
			var i;
			for (i = 0; i<classes.length;i++){
				//console.log(classes[i]);
				var elem = graphMain.getCells();
				var j;
				for (j = 0; j<elem.length;j++){
					/*console.log(elem[j].attr('text/text'));
					console.log(classes[i]);*/
					if ('"'+elem[j].attr('text/text')+'"' == classes[i]){
						markElement(elem[j]);
					}
				}
			}
		}
	} else {
	  	// Se muestran informaciones sobre el problema ocasionado durante el tratamiento de la petición
	    console.error(req.status + " " + req.statusText);
	  	}
	});
}

//para consultar a omelet
function consultarOmelet() {
	// Creación de la petición HTTP
	var req = new XMLHttpRequest();
	// Petición HTTP GET asíncrona si el tercer parámetro es "true" o no se especifica
	req.open("POST", "http://localhost:3000/", true);
	// Envío de la petición
	var q = exportJSON();
	console.log(JSON.stringify(q));
	req.send(JSON.stringify(q));

	//req.send("asd");
	// Gestor del evento que indica que la petición no ha podido llegar al servidor
	req.addEventListener("error", function(){
	  console.error("Error de red"); // Error de conexión
	});
	return req;
}

//exportar json para omelet
function exportJSON() {
    var allElement = getAllElement();
    var entities = allElement[0];
    var relationships = allElement[1];
    var attributes = allElement[2];
    var inheritances = allElement[3];
    var connectors = allElement[4];
    var elements = graphMain.getElements();
    var links = graphMain.getLinks();
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        var cardinality = "";
        var labels = link.attributes.labels;
        if (labels != null) {
            cardinality = labels[0].attrs.text.text;
        }
        //var cid = link.cid;
        //var id = cid.match(/\d+/g)[0];
        var element1 = graphMain.getCell(link.source().id);
        var element1id = element1.cid.match(/\d+/g)[0];
        var element2 = graphMain.getCell(link.target().id);
        var element2id = element2.cid.match(/\d+/g)[0];
        connectors.push({"total": false,"element1": element1id,"element2": element2id,"cardinality2": cardinality,"name": "","inheritance": false,"cardinality1": cardinality,"direction": false});
    }
    var json = {"entities":entities,"relationships":relationships,"attributes":attributes,"inheritances":inheritances,"connectors":connectors};
	return json;
}

function cleanInference() {
	//console.log(palette.model.attributes.cells.models[0]);
    var elements = graphMain.getCells();
	for (var i = 0; i < elements.length; i++) {
		unmarkElement(elements[i]);
	}
}

function markElement(element) {
	//el ISA tiene que ser si o si en polygon
	if (getType(element) != 'Inheritance') {
		element.attr('.outer/stroke','red');
	} else {
		element.attr('polygon/stroke','red');
	}
}

function unmarkElement(element) {
	//el ISA tiene que ser si o si en polygon
	var defaultStroke = getDefaultStroke(element);
	if (getType(element) != 'Inheritance') {
		element.attr('.outer/stroke',defaultStroke);
	} else {
		element.attr('polygon/stroke',defaultStroke);
	}
}

function getDefaultStroke(element) {
	//obtiene el stroke por defecto del elemento. Se basa en el valor que tiene en la paleta
	if (getType(element) != 'Inheritance') {
		for (var i = 0; i < paletteElements.length; i++) {
			if (paletteElements[i].attributes.type.includes(element.attributes.type)) {
				return paletteElements[i].attr('.outer/stroke');
			}
		}
	} else {
		for (var i = 0; i < paletteElements.length; i++) {
			if (paletteElements[i].attributes.type.includes(element.attributes.type)) {
				return paletteElements[i].attr('polygon/stroke');
			}
		}
	}
}