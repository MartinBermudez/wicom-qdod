# link.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}

# A Link between two classes.
#
# @namespace model.uml
class Link extends model.MyModel
    # @param classes {Array<Class>} An array of Class objects,
    #   the first class is the "from" and the second is the "to" class
    #   in a two-linked relation.
    constructor: (classes, name) ->
      super(name)
      @classes = classes


    hasSourceAndTarget: (name, class_a, class_b) ->
      if @classes[0] == class_a and @classes[1] == class_b and @fullname == name
        return true
      else if @classes[1] == class_a and @classes[0] == class_b and @fullname == name
              return true
           else return false


    get_classes: () ->
      return @classes

    get_from: () ->
      return classes[0]

    get_to: () ->
      return classes[1]


    # Is this link associated to the given class?
    #
    # @param c {Class instance} The class to test with.
    #
    # @return {Boolean}
    is_associated: (c) ->
        return @classes.includes(c)


    to_json: () ->
      super()

    #
    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->


    # Compare the relevant elements of these links.
    #
    # @param other {Link}
    # @return {boolean}
    same_elts: (other) ->


Link.get_new_name = () ->
    if Link.name_number == undefined
        Link.name_number = 0
    Link.name_number = Link.name_number + 1
    return "r" + Link.name_number


exports.model.Link = Link
