# diagram.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}

# Abastract class for diagrams.
#
# @namespace model
class Diagram

    #
    # @param graph [joint.Graph]. A joint Graph where all the diagram develops.
    #
    constructor: (@graph = null, owllink = "") ->
        @factory = null
        model.ns_initialize()
        @ns = model.inamespace
#        @ns.get_c_namespaces()
        @owllink = ""

    # Reset the current diagram to start over empty.
    reset: () ->

    refresh_URIs_diag: () ->

    # Get the factory used for creating the joint.dia elements.
    #
    # @return [Factory] A factory subclass instance.
    get_factory: () ->
        return @factory

    # @param factory [Factory] Used for creating the joint.dia elements.
    set_factory: (@factory) ->

    show_class_info: (class_id) ->

    get_all_namespaces: () ->
      return @ns

    get_ontologyIRI: () ->
      return @ns.get_ontologyIRI()

    get_defaultIRIs: () ->
      return @ns.get_defaultIRIs()

    get_IRIs: () ->
      return @ns.get_IRIs()

    reset_ns: () ->
      @ns.reset_ns()

    add_default_ns: (iri, prefix) ->
      @ns.add_default_ns iri, prefix

    add_custom_ns: (iri, prefix) ->
      @ns.add_custom_ns iri, prefix

    update_ontologyIRI: (iri, prefix) ->
      @ns.update_ontologyIRI(iri, prefix)

    update_lnamespaces: (arr_ns) ->
      @ns.update_lnamespaces(arr_ns)


    update_view: (class_id, paper) ->


    find_class_by_name: (name) ->


    find_class_by_classid: (classid) ->


    find_attr_by_attrid: (attrid) ->
    #
    # @return [joint.graph]
    get_graph: () ->
        return @graph


    get_namespaces: () ->
      return @ns.get_c_namespaces()


    check_names_for_IRIs: (name) ->


    # @param graph [joint.graph]
    set_graph: (@graph) ->

    # Update the joint.Graph instance with the new cells.
    actualizar_graph: () ->

    # Return a JSON representing the Diagram.
    #
    # We want to send only some things not all the JSON the object
    # has. So, we drop some things, like JointJS graph objects.
    #
    # Use JSON.stringify(json_object) to translate a JSON object to
    # string.
    #
    # @return [object] A JSON object.
    to_json: () ->
      @ns.get_c_namespaces()

    # Import the diagram in JSON format.
    #
    # @param json [object] A JSON object.
    import_json: (json) ->


# Create an namespace object for initializing current diagram.

# @namespace model
exports.model.ns_initialize = () ->
  exports.model.inamespace = new model.Namespaces()
  gui.gui_instance.widgets.refresh_ns(model.inamespace)

exports.model.Diagram = Diagram
