# namespaces.coffee --
# Copyright (C) 2018 Giménez, Christian - Braun, Germán

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}


model.root = exports ? this
model.root.uripatternbar = () -> ///^((http|https):\/\/([a-zA-Z0-9\/\.\-\_])+\/([a-zA-Z0-9])+)$///i
model.root.uripatternhash = () -> ///^((http|https):\/\/([a-zA-Z0-9\/\.\-\_])+\#([a-zA-Z0-9])+)$///i
model.root.prefixpatternname = () -> ///([a-zA-Z0-9])+\:([a-zA-Z0-9])+///i

# Abastract class for diagrams.
#
# @namespace model
class Namespaces


#{"ontologyIRI" : {
#   "prefix" : "crowd",
#   "value" : "http://crowd.fi.uncoma.edu.ar/"
#   },
# "defaultIRIs" :
#     [
#     {"prefix" : "owl", "value" : "http://www.w3c.org#"}
#   ],
# "IRIs" :
#     [{"prefix" : "dwc", "value" : "http://www.dwc.org#"}]
#}

    d_namespaces: {"ontologyIRI" :
                        [{"prefix" : "crowd", "value" : "http://crowd.fi.uncoma.edu.ar#"}]
                   "defaultIRIs" :
                        [{"prefix" : "rdf", "value" : "http://www.w3.org/1999/02/22-rdf-syntax-ns#"},
                         {"prefix" : "rdfs", "value" : "http://www.w3.org/2000/01/rdf-schema#"},
                         {"prefix" : "xsd", "value" : "http://www.w3.org/2001/XMLSchema#"},
                         {"prefix" : "owl", "value" : "http://www.w3.org/2002/07/owl#"}]
                    "IRIs" : []
                  }

    c_namespaces: {"ontologyIRI" :
                            []
                   "defaultIRIs" :
                            []
                   "IRIs" :
                            []
                  }


    constructor: (inst_ns = null) ->
      if not inst_ns?
        Namespaces::c_namespaces = Namespaces::d_namespaces
      else Namespaces::c_namespaces = inst_ns

    get_c_namespaces: () ->
      return Namespaces::c_namespaces

    reset_ns: () ->
      Namespaces::c_namespaces = {"ontologyIRI" :
                                          []
                                  "defaultIRIs" :
                                          []
                                  "IRIs" :
                                          []
                                 }

    # Get the list of namespaces used in an ontology.
    #
    # @return [lnamespace] A list of links and their prefixes.
#    get_lnamespace: () ->
#        return @get_c_namespaces()

    get_prefix_str: (elt) ->
        return elt["prefix"]

    get_url_str: (elt) ->
        return elt["value"]

    # Get the current ontologyIRI
    #
    # @return [ontologyIRI] An array containing the ontologyIRI and its prefix.
    get_ontologyIRI: () ->
        obj = JSON.parse(JSON.stringify(this.get_c_namespaces()))
        return obj.ontologyIRI[0]

    # Get a reference to the URI (prefix, link) in the current namespace.
    #
    # @param prefix, a URI prefix
    # @param link, a URI value. A URL.
    link_URI_to_namespace: (prefix, link) ->
      obj = null

      obj = @get_c_namespaces()["ontologyIRI"].find((elt, index) ->
        elt["prefix"] == prefix and elt["value"] == link
        )

      if not obj?
        obj = @get_c_namespaces()["defaultIRIs"].find((elt, index) ->
          elt["prefix"] == prefix and elt["value"] == link
          )

        if not obj?
          obj = @get_c_namespaces()["defaultIRIs"].find((elt, index) ->
            elt["prefix"] == prefix and elt["value"] == link
            )

      return obj


    get_ontologyIRI_prefix: () ->
        return this.get_ontologyIRI().prefix

    get_ontologyIRI_value: () ->
        return this.get_ontologyIRI().value

    is_ontologyIRI_value: (uri) ->
        return this.get_ontologyIRI_value() == uri

    is_ontologyIRI_prefix: (prefix) ->
        return this.get_ontologyIRI_prefix() == prefix

    get_defaultIRIs: () ->
        obj = JSON.parse(JSON.stringify(this.get_c_namespaces()))
        return obj.defaultIRIs

    get_default_prefixes: () ->
        p = []
        d = this.get_defaultIRIs()

        d.forEach (elem, index, d) ->
            p.push(d[index]["prefix"])

        return p

    get_default_values: () ->
        v = []
        d = this.get_defaultIRIs()

        d.forEach (elem, index, d) ->
            v.push(d[index]["value"])

        return v

    get_custom_IRIs: () ->
        obj = JSON.parse(JSON.stringify(this.get_c_namespaces()))
        return obj.IRIs


    get_custom_prefixes: () ->
        p = []
        d = this.get_custom_IRIs()

        d.forEach (elem, index, d) ->
            p.push(d[index]["prefix"])

        return p

    get_custom_values: () ->
        v = []
        d = this.get_custom_IRIs()

        d.forEach (elem, index, d) ->
            v.push(d[index]["value"])

        return v

    # @param link. A String for a default IRI.
    # @param abbr. A String for a default prefix.
    add_default_ns: (link, abbr) ->
        @get_c_namespaces()["defaultIRIs"].push({prefix : abbr, value : link})

    # @param link. A String for a custom IRI.
    # @param abbr. A String for a custom prefix.
    add_custom_ns: (link, abbr) ->
        @get_c_namespaces()["IRIs"].push({prefix : abbr, value : link})

    update_ontologyIRI: (link, abbr) ->
      if @get_c_namespaces()["ontologyIRI"].length > 0
        @get_c_namespaces()["ontologyIRI"][0]["prefix"] = abbr
        @get_c_namespaces()["ontologyIRI"][0]["value"] = link
      else
        @get_c_namespaces()["ontologyIRI"].push({prefix : abbr, value : link})

    # This function updates the list of prefixes checking for ns and values of each one
    #
    # @param arr. An array of prefixes as {prefix: p, iri: url}
    update_lnamespaces: (arr_ns) ->

      arr_ns.forEach (elem, index, arr_ns) =>
        d_ns_prfx = null
        d_ns_val = null
        c_ns_prfx = null
        c_ns_val = null
        d_ns_prfx = this.find_default_ns_by_prefix(arr_ns[index]["prefix"])
        d_ns_val = this.find_default_ns_by_url(arr_ns[index]["value"])
        c_ns_prfx = this.find_custom_ns_by_prefix(arr_ns[index]["prefix"])
        c_ns_val = this.find_custom_ns_by_url(arr_ns[index]["value"])

        # neither prefix nor url belong to the lnamespace
        if (d_ns_prfx == undefined) && (d_ns_val == undefined) &&
           (c_ns_prfx == undefined) && (c_ns_val == undefined)
          this.add_custom_ns(arr_ns[index]["value"], arr_ns[index]["prefix"])
        else # if arr matches prefix then check associated iri
          if (d_ns_prfx != undefined) && (d_ns_val == undefined) &&
             (c_ns_prfx == undefined) && (c_ns_val == undefined)
            this.replace_default_ns_by_prefix(arr_ns[index]["prefix"], arr_ns[index]["value"])
          else # if arr matches iri then check associated prefix
            if (d_ns_prfx == undefined) && (d_ns_val != undefined) &&
               (c_ns_prfx == undefined) && (c_ns_val == undefined)
              this.replace_default_ns_by_url(arr_ns[index]["prefix"], arr_ns[index]["value"])
            else # if arr matches prefix then check associated iri
              if (d_ns_prfx == undefined) && (d_ns_val == undefined) &&
                 (c_ns_prfx != undefined) && (c_ns_val == undefined)
                this.replace_custom_ns_by_prefix(arr_ns[index]["prefix"], arr_ns[index]["value"])
              else # if arr matches iri then check associated prefix
                if (d_ns_prfx == undefined) && (d_ns_val == undefined) &&
                   (c_ns_prfx == undefined) && (c_ns_val != undefined)
                  this.replace_custom_ns_by_url(arr_ns[index]["prefix"], arr_ns[index]["value"])

    replace_default_ns_by_prefix: (prefix, iri) ->
      @get_c_namespaces()["defaultIRIs"].forEach (elem, index) ->
        if (elem["prefix"] == prefix)
          elem["value"] = iri

    replace_default_ns_by_url: (prefix, iri) ->
      @get_c_namespaces()["defaultIRIs"].forEach (elem, index) ->
        if (elem["value"] == iri)
          elem["prefix"] = prefix

    replace_custom_ns_by_prefix: (prefix, iri) ->
      @get_c_namespaces()["IRIs"].forEach (elem, index) ->
        if (elem["prefix"] == prefix)
          elem["value"] = iri

    replace_custom_ns_by_url: (prefix, iri) ->
      @get_c_namespaces()["IRIs"].forEach (elem, index) ->
        if (elem["value"] == iri)
          elem["prefix"] = prefix

    find_default_ns_by_prefix: (prefix) ->
      return @get_c_namespaces()["defaultIRIs"].find((elt, index) ->
        elt["prefix"] == prefix
        )

    find_default_ns_by_url: (iri) ->
      return @get_c_namespaces()["defaultIRIs"].find((elt, index) ->
        elt["value"] == iri
        )

    find_custom_ns_by_prefix: (prefix) ->
      return @get_c_namespaces()["IRIs"].find((elt, index) ->
        elt["prefix"] == prefix
        )

    find_custom_ns_by_url: (iri) ->
      return @get_c_namespaces()["IRIs"].find((elt, index) ->
        elt["value"] == iri
        )


    # Check if class name includes an IRI and return true if that IRI name has been defined in the current diagram
    # if IRI name is not existing then it is added to namespaces
    #
    # @param name. A String of a class name
    # @return A name without URI or null otherwise
    has_fullname: (name) ->
      if name?
        nname = null

        if (name.match uripatternbar()) || (name.match uripatternhash())
          nname = this.get_name(name)
          return nname
        else
          return nname
      else return name

    get_name: (fullname) ->
      if fullname?
        name = null
        len = fullname.length

        if fullname.match uripatternbar()
          bar = fullname.lastIndexOf '/'
          name = fullname.slice bar + 1, len
        else
          if fullname.match uripatternhash()
            hash = fullname.lastIndexOf '#'
            name = fullname.slice hash + 1, len

        return name

    get_URL: (fullname) ->
      url = null
      name = null
      name = this.has_fullname(fullname)

      if name?
        len = fullname.length

        if fullname.match uripatternbar()
          bar = fullname.lastIndexOf '/'
          url = fullname.slice 0, bar + 1
        else
          if fullname.match uripatternhash()
            hash = fullname.lastIndexOf '#'
            url = fullname.slice 0, hash + 1

      return url

    get_prefix: (fullname) ->
      if fullname?
        name = null
        len = fullname.length
        dot = fullname.lastIndexOf ':'

        if dot > 0
          name = fullname.slice 0, dot
        return name
      return fullname

    get_name_from_prefix: (namep) ->
      if namep?
        name = null
        len = namep.length
        dot = namep.lastIndexOf ':'
        name = namep.slice dot + 1, len

        return name

    has_prefix: (name) ->
      iri = null

      if name.match prefixpatternname()
        iri = null
        abbr = this.get_prefix(name)
        iri = this.find_default_ns_by_prefix(abbr)

        if iri?
          return iri["value"]
        else
          iri = this.find_custom_ns_by_prefix(abbr)

          if iri?
            return iri["value"]
          else
            if this.is_ontologyIRI_prefix(abbr)
              iri = this.get_ontologyIRI_value()
              return iri
            else return iri


exports.model.Namespaces = Namespaces

# Default Namespaces instance
#exports.model.inamespace = new Namespaces()
