# link.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A Link between two classes.
#
# @namespace model.uml
class Attribute extends model.Link
    # @param classes {Array<Class>} An array of Class objects,
    #   the first class is the "from" and the second is the "to" class
    #   in a two-linked relation.
    constructor: (classes, name = null, type = null) ->
        if name?
            super(classes, name)
        else
            super(classes, Attribute.get_new_name())

        @datatype = new model.uris.URI()
        @datatype.set_URI(type)


    get_datatype: () ->
      return @datatype


    get_class_attr: () ->
      return @classes


    to_json: () ->
        json = super()
        json.classes = $.map(@classes, (myclass) ->
            myclass.get_fullname()
        )
        json.multiplicity = [@role_from.get_mult(), @role_to.get_mult()]
        roles = [@role_from.get_fullname(), @role_to.get_fullname()]

        json.roles = roles
        json.type = "association"

        return json

    #
    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if @joint == null
            @joint = []
            if csstheme != null
                @joint.push(factory.create_binary_association(
                    @classes[0].get_classid(),
                    @classes[1].get_classid(),
                    @name,
                    csstheme.css_links,
                    @role_from.get_mult(),
                    @role_to.get_mult()))
            else
                @joint.push(factory.create_binary_association(
                    @classes[0].get_classid(),
                    @classes[1].get_classid(),
                    @name
                    null,
                    @role_from.get_mult(),
                    @role_to.get_mult()))



    # Compare the relevant elements of these links.
    #
    # @param other {Link}
    # @return {boolean}
    same_elts: (other) ->
        if !super(other)
            return false
        if @classes.length != other.classes.length
            return false

        # it must have the same order!
        all_same = true
        for index in [0...@classes.length]
            do (index) =>
                all_same = all_same && @classes[index].same_elts(other.classes[index])

        all_same = all_same && v == other.mult[k] for v,k in @mult
        all_same = all_same && v == other.roles[k] for v,k in @roles

        return all_same


Attribute.get_new_name = () ->
    if Attribute.name_number == undefined
        Attribute.name_number = 0
    Attribute.name_number = Attribute.name_number + 1
    return "a" + Attribute.name_number


exports.model.uml.Attribute = Attribute
