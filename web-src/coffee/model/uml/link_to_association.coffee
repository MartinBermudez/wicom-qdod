# generalization.coffee --
# Copyright (C) 2018 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A link to associationIcon.
#
# @namespace model.uml
class LinkToAssociation extends model.Link

    # @param classes {Class} Classes involved in this association.
    constructor: (classes, name = null, b_role) ->
        super(classes, name)
        @role = b_role


    get_link_role: () ->
      return @role

    set_link_role: (role) ->
      @role = role


    change_to_null : (mult, index) ->
        if (mult == "0..*") or (mult == "0..n")
            mult[index] = null


    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_to_association_link(
                @classes[0].get_classid(),
                @classes[1].get_relid(),
                @name,
                csstheme.css_links,
                @role.get_mult(),
                @role.get_name(),
                ))


    to_json: () ->

exports.model.uml.LinkToAssociation = LinkToAssociation
