exports = exports ? this
exports.model = exports.model ? {}
exports.model.eer = exports.model.eer ? {}
# @namespace model.eer
class LinkEntityToRel extends model.eer.LinkAttrToEntity
    constructor: (classes, name = null,@mult,@roles) ->
        super(classes, name)
        this.set_mult(@mult)
        this.set_roles(@roles)

    # Set the multiplicity.
    # @param String mult A String that describes the multiplicity. The mult from the class associated with relationIcon
    set_mult : (@mult) ->
        if (@mult == "0..*") or (@mult == "0..n") or  (@mult == "")
          @mult=null


    # Set the role.
    #
    # @param [String,String] roles. The role from the class associated with relationIcon. First string is the user defined role and second is default
    set_roles: (@roles) ->
      if (@roles[0] == "") or (@roles[0] == undefined)
        @roles[0]=null

    change_to_null : (mult, index) ->
        if (mult == "0..*") or (mult == "0..n")
            mult[index] = null

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_to_relation_link(
                @classes[0].get_relid(),
                @classes[1].get_classid(),
                @name,
                csstheme.css_links,
                @mult,
                @roles))

      to_json: () ->

exports.model.eer.LinkEntityToRel = LinkEntityToRel
