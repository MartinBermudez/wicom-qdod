<?php 
/* 

   Copyright 2016 Giménez, Christian
   
   Author: Giménez, Christian   

   errorwidget.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade" id="error_widget" tabindex="-1" role="dialog"
     aria-labelledby="error_widget" aria-hidden="true">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header alert alert-danger">
		<h1 class="modal-title"> Error: </h1>
		<button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">
		<dl>
		    <dt>Status:</dt><dd>
			<div id="errorstatus_text"></div>
		    </dd>
		    <dt>Server Answer:</dt><dd>
			<pre>
			    <div id="errormsg_text"></div>
			</pre>
		    </dd>
		</dl>
	    </div>

	    <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">
		    Hide
		</button>
	    </div>
	</div>
    </div>
</div>
