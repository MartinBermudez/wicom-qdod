# clear_widget.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? {}


ClearWidget = Backbone.View.extend(
    initialize: () ->
        this.render()

    render: () ->
        template = _.template( $("#template_clear").html() )
        this.$el.html( template() )

    events:
        'click button#clear_yes_btn': 'do_yes'

    do_yes: () ->
        $("#clear_widget").modal("hide")
        gui.gui_instance.current_gui.diagadapter.reset_all()

)


exports.views.common.ClearWidget = ClearWidget

