exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? {}


ReasoningDetailsWidget = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template( $("#template_reasoning").html() )
        this.$el.html(template({}))


    set_kb: (satisfiable_kb) ->
      $("#reasoner_kb").val("")
      $("#reasoner_kb").val(satisfiable_kb)

    set_reasoner_output: (reasoner_output) ->
      $("#reasoner_output").val("")
      $("#reasoner_output").val(reasoner_output)

    set_sat: (satisfiable) ->
      $("#reasoner_sat").html("")
      $("#reasoner_sat").html(satisfiable)

    set_unsat : (unsatisfiable) ->
      $("#reasoner_unsat").html("")
      $("#reasoner_unsat").html(unsatisfiable)

    show: () ->
        this.$el.children(0).modal("show")

    hide: () ->
        this.$el.children(0).modal("hide")
    )


exports.views.common.ReasoningDetailsWidget = ReasoningDetailsWidget
