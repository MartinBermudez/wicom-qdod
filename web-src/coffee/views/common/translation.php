<div class="modal fade" id="translation_widget" tabindex="-1" role="dialog"
     aria-labelledby="translation_widget" aria-hidden="true">

    <div class="modal-dialog">
	<div class="modal-content">
	    
	    <div class="modal-header">
		<h3 class="modal-title">Translation Output</h3>
		<button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">		
		<div id="html-output"></div>
	
		<textarea class="form-control" cols="10"
			  id="owllink_source"></textarea>
	    </div>

	    <div class="modal-footer">
		<div class="btn-group" role="group">
		    <button class="btn btn-secondary" type="button"
			    id="translate_button"
				   onclick="guiinst.translate_owllink()">
			Translate Again
		    </button>
		    <button type="button" class="btn btn-primary"
			    data-dismiss="modal">
			Hide
		    </button>
		</div>
	    </div>
    </div>
</div>
