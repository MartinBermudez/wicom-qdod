# namespaces.coffee --
# Copyright (C) 2018 Giménez, Christian. Braun, germán

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.common = exports.views.common ? this


# View widget for the "Import JSON".
NamespacesView = Backbone.View.extend(
    initialize: () ->
        this.render()
#        @ns = model.inamespace
        # For some reason, backboneJS doesn't process the given event.
        # Maybe a bug or some incompatibility with JQueryMobile?
        # $("#namespaces_btn").on 'click', this.add_namespace

    render: () ->
        template = _.template( $("#template_namespaces").html() )
        this.$el.html( template() )

    events:
        "click button#accept_namespaces_btn": "add_namespace"

    # This function loads default IRIs for the current ontology
    #
    # @param ns an namespace object
    set_namespace: (@ns = model.inamespace) ->
        @refresh_namespace(@ns)

    # Refresh the widget with the namespace data.
    refresh_namespace: (@ns = model.inamespace) ->
        ontologyIRI_p = @ns.get_ontologyIRI_prefix()
        ontologyIRI_v = @ns.get_ontologyIRI_value()
        p_default = @ns.get_default_prefixes()
        v_default = @ns.get_default_values()
        p_custom = @ns.get_custom_prefixes()
        v_custom = @ns.get_custom_values()

        prefixes = p_default.concat p_custom
        values = v_default.concat v_custom

        $("#ontoiri_prefix").val ontologyIRI_p
        $("#ontoiri_value").val ontologyIRI_v

        prefixes.forEach (elem, index, prefixes) ->
            i = "#nsp".concat index + 1
            input = i.concat "_input"
            $(input).val prefixes[index]

        values.forEach (elem, index, values) ->
            v = "#nsv".concat index + 1
            value = v.concat "_input"
            $(value).val values[index]


    add_namespace: () ->
        @hide()

        ontoIRIp = $("#ontoiri_prefix").val()
        ontoIRIv = $("#ontoiri_value").val()

        @ns.update_ontologyIRI ontoIRIv, ontoIRIp

        arr_ns = []
        unless $("#nsp1_input").val() == "" && $("#nsv1_input").val() == ""
            arr_ns.push
                prefix : $("#nsp1_input").val()
                value  : $("#nsv1_input").val()

        unless $("#nsp2_input").val() == "" && $("#nsv2_input").val() == ""
            arr_ns.push
                prefix : $("#nsp2_input").val()
                value  : $("#nsv2_input").val()

        unless $("#nsp3_input").val() == "" && $("#nsv3_input").val() == ""
            arr_ns.push
                prefix : $("#nsp3_input").val()
                value  : $("#nsv3_input").val()

        unless $("#nsp4_input").val() == "" && $("#nsv4_input").val() == ""
            arr_ns.push
                prefix : $("#nsp4_input").val()
                value  : $("#nsv4_input").val()

        unless $("#nsp5_input").val() == "" && $("#nsv5_input").val() == ""
            arr_ns.push
                prefix : $("#nsp5_input").val()
                value  : $("#nsv5_input").val()

        unless $("#nsp6_input").val() == "" && $("#nsv6_input").val() == ""
            arr_ns.push
                prefix : $("#nsp6_input").val()
                value  : $("#nsv6_input").val()

        unless $("#nsp7_input").val() == "" && $("#nsv7_input").val() == ""
            arr_ns.push
                prefix : $("#nsp7_input").val()
                value  : $("#nsv7_input").val()

        unless $("#nsp8_input").val() == "" && $("#nsv8_input").val() == ""
            arr_ns.push
                prefix : $("#nsp8_input").val()
                value  : $("#nsv8_input").val()

        unless $("#nsp9_input").val() == "" && $("#nsv9_input").val() == ""
            arr_ns.push
                prefix : $("#nsp9_input").val()
                value  : $("#nsv9_input").val()

        @ns.update_lnamespaces arr_ns
        gui.uml.iguiuml.diag.refresh_URIs_diag()


#    show: () ->
#      this.$el.children(0).modal "show"

    hide: () ->
        this.$el.children(0).modal "hide"
)

exports.views.common.NamespacesView = NamespacesView
