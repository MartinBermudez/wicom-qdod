<?php
/*

   Copyright 2016 GILIA

   Author: GILIA

   relationoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="geniconOptions" style="visible:false, z-index:1, position:absolute" >
    <input type="hidden" id="voptions_geniconid" name="geniconid" value="<%= geniconid %>" />

<div class="btn-group btn-group-sm" role="group">
    <button class="btn btn-sm btn-primary" type="button" id="umladdchild_button">
  Add Child
    </button>
    <button class="btn btn-sm btn-secondary" type="button" id="umldisjoint_button">
  Disjoint
    </button>
    <button class="btn btn-sm btn-secondary" type="button" id="umlcovering_button">
  Covering
    </button>
    <button type="button" class="btn btn-sm btn-danger" id="umldeletegeneralization_button">
  Delete
    </button>
    <button class="btn btn-sm btn-info" type="button" title="i" id="metadata_button">
    Info
    </button>
</div>
</div>
