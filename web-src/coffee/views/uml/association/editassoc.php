<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   edit_attributes.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>


<div class="editAssoc">
    <input type="hidden" id="umlrelationoptions_classid"  name="classid"  value="<%= classid %>" />

    <form id="name-rel">
	<input type="hidden" id="umlrelationoptions_classid"
	       name="classid" value="<%= classid %>" />
	<input type="hidden" id="umlrelationoptions_classid"
	       name="classid" value="<%= classid %>" />

        <div class="input-group">
            <input class="form-control" placeholder="0"
		   type="text" id="narycardto-1" size="2" maxlength="4" />
            <input class="form-control" placeholder="*"
		   type="text" id="narycardto-2" size="2" maxlength="4" />
        </div>
	
	<input class="form-control" placeholder="Role Name"
	       type="text" size="4" id="naryrole-to" />
	<input class="form-control" placeholder="Prefix"
	       type="text" id="umlrole_a_editprefix_input" value="" />
	<input class="form-control" placeholder="URL"
	       type="text" id="umlrole_a_editurl_input"  value=""/>

        <div class="btn-group">
            <button class="btn btn-primary" type="button"
		    id="done_button">Done</button>
	    <button class="btn btn-danger" type="button"
		    id="uml_close_button">Close</button>
        </div>
    </form>
    
</div>
