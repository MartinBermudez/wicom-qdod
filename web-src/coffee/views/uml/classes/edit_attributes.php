<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   edit_attributes.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="editAssoc">
    <input type="hidden" id="editattr_classid"  name="classid"  value="<%= classid %>" />

<form id="umlattr_list">

    <div id="container">
        <div class="dropdown">
          <select class="datatypes" id="builtin_datatypes">
              <option value="xsd_string">xsd:string</option>
              <option value="xsd_boolean">xsd:boolean</option>
              <option value="xsd_integer">xsd:integer</option>
              <option value="xsd_dateTime">xsd:dateTime</option>
              <option value="xsd_decimal">xsd:decimal</option>
          </select>
        </div>
    </div>
    <input class="form-control" placeholder="Attribute Name"
  	       type="text" size="4" id="umlattr_input" />
  	<input class="form-control" placeholder="Prefix"
  	       type="text" id="umlattr_a_editprefix_input" value="" />
  	<input class="form-control" placeholder="URL"
  	       type="text" id="umlattr_a_editurl_input"  value=""/>


    <div class="btn-group" role="group">
	<button class="btn btn-sm btn-primary" type="button"
		id="done_button">
	    Add
	</button>
	<button class="btn btn-sm btn-danger"  type="button"
		id="uml_close_button">
	    Close
	</button>
    </div>
</form>
</div>
