<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   tools_uml.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="navbar-nav">
    <div class="nav-item dropdown">
	<a role="button" class="nav-link dropdown-toggle" href="#"
	   id="uml-dropdown-btn" data-toggle="dropdown"
	   aria-haspopup="true" aria-expanded="false">
	    Reasoning
	</a>
	<div class="dropdown-menu" aria-labelledby="uml-dropdown-btn">
	    <a class="dropdown-item" href="#" id="menu-translate-uml">
		UML Encoding
	    </a>
	    <a class="dropdown-item" href="#" id="menu-check-consistency">
		Reasoning over model
	    </a>
	</div>
    </div>

    <a class="nav-link" id="umlclass_button" href="#">Class</a>
</div>

<!--
     <a class="btn btn-secondary" id="umlassoc_button">Assoc</a>
     <a class="btn btn-secondary" id="umlisa_button">IS-A</a>
   -->
