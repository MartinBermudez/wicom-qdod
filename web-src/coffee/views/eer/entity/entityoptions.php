<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   classoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="classOptions">
    <input type="hidden" id="cassoptions_classid"
	   name="classid" value="<%= classid %>" />

    <div class="btn-group btn-sm" role="group"
	 style="visible:false, z-index:1, position:absolute" >
	
	<button class="btn btn-primary btn-sm" type="button"
		id="eereditclass_button">
	    Edit
	</button>
	<button type="button" class="btn btn-secondary btn-sm"
		id="eerisa_btn">
	    Is-A
	</button>
	<button type="button" class="btn btn-secondary btn-sm"
		id="eerrelation_btn">
	    Relation
	</button>
	<button class="btn btn-danger btn-sm" type="button"
		id="eerdeleteclass_button">
	    Delete
	</button>
	
    </div>
</div>
