<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   editclass.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<form>
    <input type="hidden" id="editclass_classid"
	   name="classid" value="<%= classid %>" />
    <input placeholder="ClassName" type="text"
	   class="form-control" id="editclass_input"  />
    <div class="btn-group" role="group">
	<button class="btn btn-primary btn-sm" type="button"
		id="eereditclass_button">
	    Accept
	</button>
	<button class="btn btn-secondary btn-sm" type="button"
		id="eerclose_button">
	    Close
	</button>
    </div>
</form>
