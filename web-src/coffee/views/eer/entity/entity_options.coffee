# entity_options.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? {}
exports.views.eer = exports.views.eer ? {}
exports.views.eer.entity = exports.views.eer.entity ? {}

# @namespace views
EntityOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_entityoptions").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#eerdeleteclass_button" : "delete_class",
        "click button#eereditclass_button" : "edit_class"
        "click button#eerisa_btn": "eerisa"
        "click button#eerrelation_btn": "eerrelation"

    ##
    # Set the classid of the Joint Model associated to this EditClass
    # instance, then set the position of the template to where is the
    # class Joint Model.
    set_classid: (@classid) ->

        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x,
            position: 'absolute',
            'z-index': 1
            )
        # this.$el.show()

    ##
    # Return the ClassID of the Joint.Model element associated to
    # this EditClass instance.
    get_classid: () ->
        return @classid

    delete_class: (event) ->
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.diagadapter.delete_class(@classid)

    edit_class: (event) ->
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.widgets.editclass.set_classid(@classid)
        gui.gui_instance.current_gui.widgets.editclass.show()
        this.hide()

    eerisa: (event) ->
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.widgets.isaoptions.set_classid(@classid)
        gui.gui_instance.current_gui.widgets.isaoptions.show()
        this.hide()

    eerrelation: (event) ->
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.widgets.relationoptions.set_classid(@classid)
        gui.gui_instance.current_gui.widgets.relationoptions.show()
        this.hide()

    show: () ->
        this.$el.show()

    hide: () ->
        this.$el.hide()
)


exports.views.eer.entity.EntityOptionsView = EntityOptionsView
