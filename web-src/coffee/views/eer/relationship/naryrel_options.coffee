exports = exports ? this
exports.views = exports.views ? this
exports.views.eer = exports.views.eer ? this
exports.views.eer.relationship = exports.views.eer.relationship ? this

NaryRelOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_naryrel_options").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click a#naryrel_button" : "new_relation",
        "click a#naryrel_attr_button" : "new_rel_attr"

    # Create a new relationship with the information from the role, multiplicity and
    # relationship name input fields.
    #
    # Callback, used when the user clicks on the relationship button.
    new_relation: () ->
        mult = []
        mult[0] = ""
        mult[1] = ""
        roles = []
        roles[0] = ""
        roles[1] = ""
        name = $("#naryrel_name").val()
        if name==""
          name=null
        gui.gui_instance.set_association_state(@classid, mult, roles, [name,"nary"])

    # Create a new relation with the information from the role, multiplicity and
    # relationship name input fields. *Use an relationship class in the middle.*
    #
    # Callback, used when the user clicks on the create relationship class button.
    new_rel_attr: (from, too) ->
        mult = []
        mult[0] = ""
        mult[1] = ""
        roles = []
        roles[0] = ""
        roles[1] = ""
        name = $("#nary_name").val()
        if name==""
          name=null
        @hide()
        gui.gui_instance.set_association_state(@classid, mult, roles, [name,"nary"])


    # Map the Option value to multiplicity string.
    #
    # @param str {string} The value string.
    # @return {String} A string that represent the multiplicity as in UML.
    map_to_mult : (str) ->
        switch str
            when "zeromany" then "0..*"
            when "onemany" then "1..*"
            when "zeroone" then "0..1"
            when "oneone" then "1..1"


    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.eer.relationship.NaryRelOptionsView = NaryRelOptionsView
