<div class="naryrelOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="eerrelationoptions_classid"  name="classid"  value="<%= classid %>" />
    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: left">
	     <form id="name-rel">
	        <input data-mini="true" placeholder="Rel. Name" type="text" size="7" maxlength="10" id="naryrel_name" />
	        <div data-role="controlgroup" data-mini="true" data-type="horizontal">
		          <a class="ui-btn ui-mini ui-icon-arrow-r ui-btn-icon-right ui-corner-all" type="button" id="naryrel_button">Relationship</a>
		          <?php //<a class="ui-btn ui-corner-all ui-btn-icon-notext" type="button" id="naryrel_attr_button">Relation Attribute</a>?>
	        </div>
	     </form>
    </div>
</div>
