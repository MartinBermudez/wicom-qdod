<div class="reliconOptions" data-role="controlgroup" data-mini="true"
     data-type="horizontal" style="visible:false, z-index:1, position:absolute" >
    <input type="hidden" id="voptions_reliconid" name="reliconid" value="<%= reliconid %>" />
    <a class="ui-btn ui-corner-all ui-icon-edit ui-btn-icon-notext" type="button" title="Edit Relationship" id="edit_button">Edit Relationship</a>
    <a class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" type="button" title="Delete Relationship" id="deleterel_button">Delete Relationship</a>
    <a class="ui-btn ui-mini ui-icon-plus ui-btn-icon-left ui-corner-all" type="button" title="Add Class" id="addclass_button">Class</a>
    <a class="ui-btn ui-mini ui-icon-plus ui-btn-icon-left ui-corner-all" type="button" title="Add Attribute" id="addattr_button">Attr</a>
</div>
