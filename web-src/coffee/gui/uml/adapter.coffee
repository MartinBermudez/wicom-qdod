# adapter.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.uml = exports.gui.uml ? {}


# @namespace gui.uml
#
# DiagAdapter for UML diagrams.
class UMLAdapter extends gui.DiagAdapter
    constructor: (diag, paper) ->
        super(diag, paper)
        @targetCells = []


    show_class_info: (class_id) ->
      @diag.show_class_info(class_id)

    show_gen_info: (gen_id) ->
      @diag.show_gen_info(gen_id)

    show_assoc_info: (assoc_id) ->
      @diag.show_assoc_info(assoc_id)

    #
    # Add a class to the diagram.
    #
    # @param hash_data {Hash} data information for creating the Class. Use `name`, `attribs` and `methods` keys.
    # @see Class
    # @see Diagram#add_class
    add_object_type: (hash_data) ->
        gui.gui_instance.hide_toolbar()
        @diag.add_class(hash_data)


    add_attribute: (class_id, name, type) ->
        @diag.add_attribute(class_id, name, type)

    #
    # Add a simple association from A to B.
    # Then, set the selection state for restoring the interface.
    #
    # @example Getting a classid
    #   < graph.getCells()[0].id
    #   > "5777cd89-45b6-407e-9994-5d681c0717c1"
    #
    # @param class_a_id {string}
    # @param class_b_id {string}
    # @param name {string} optional. The association name.
    # @param mult {array} optional. An array of two string with the cardinality from class and to class b.
    add_relationship: (class_a_id, class_b_id, name = null, mult = null, roles = null, assoc_t) ->
        if assoc_t == "binary"
          @diag.add_association class_a_id, [class_b_id], name, mult, roles
          @state = gui.gui_instance.set_selection_state()
        else
          @targetCells.push(class_b_id)

    add_association_class: (class_a_id, class_b_id, name = null, mult = null, roles = null, assoc_t) ->
      if assoc_t == "binary"
        @diag.add_association_class(class_a_id, [class_b_id], name, mult, roles)
        @state = gui.gui_instance.set_selection_state()
      else
        @targetCells.push(class_b_id)

    add_relationship_aux: (class_a_id, name = null, mult = null, roles = null, with_class = null) ->
        if with_class
          @diag.add_association_class(class_a_id, @targetCells, name, mult, roles)
        else
          @diag.add_association(class_a_id, @targetCells, name, mult, roles)

        @targetCells = []
        @state = gui.gui_instance.set_selection_state()


    # Add a Generalization link for class parent.
    #
    # @param class_parent_id {string} The parent class Id.
    # @param class_child_id {string} The child class Id.
    #
    # @todo Support various children on parameter class_child_id.
    add_subsumption: (class_parent_id, class_child_id, disjoint = false, covering = false) ->
      @targetCells.push class_child_id

    # Get the selected child for building Generalization link.
    #
    # @param class_parent_id {string} The parent class Id.
    # @param class_child_id {string} The child class Id.
    #
    # @todo Support various children on parameter class_child_id.
    add_subsumption_childs: (class_parent_id) ->
      state = gui.state_inst.isa_state()
      disjoint = state.get_disjoint()
      covering = state.get_covering()

      @diag.add_generalization class_parent_id, @targetCells, disjoint, covering
      @targetCells = []
      @state = gui.gui_instance.set_selection_state()

    # Delete a class/generalization/association from the diagram.
    #
    # @param class_id {string} a String with the class/generalization/association Id.
    delete_class: (class_id) ->
        if @diag.find_class_by_classid(class_id) != undefined
          @diag.delete_class_by_classid(class_id)
        else
          if @diag.find_generalization_by_id(class_id) != undefined
            @diag.delete_generalization_by_id(class_id)
          else
            if @diag.find_association_by_id(class_id) != undefined
              @diag.delete_association_by_id(class_id)


    edit_covering: (gen_id, covering) ->
      @diag.edit_covering gen_id, covering
      @diag.update_gen_view gen_id, @paper

    edit_disjoint: (gen_id, disjoint) ->
      @diag.edit_disjoint gen_id, disjoint
      @diag.update_gen_view gen_id, @paper


# Unique instance of UMLAdapter.
#
# @namespace gui.uml
exports.gui.uml.iumladapter = null
exports.gui.uml.UMLAdapter = UMLAdapter
