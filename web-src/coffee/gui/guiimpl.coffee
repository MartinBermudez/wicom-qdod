# guiimpl.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.gui = exports.gui ? {}


# @namespace gui
#
# Central GUI *do-it-all* class...
#
class GUIIMPL
    constructor: (@graph = null, @paper = null) ->
        @diag = null
        @diagadapter = null

    disable: () ->
        @diagadapter.reset_all()


    enable: () ->
        # @diagadapter.import_json @diag.to_json
        @widgets.enable()

        # @param {JointJS.Graph } graph The JointJS Graph used for drawing models.
    set_graph: (graph) ->
        @graph = graph
        if @diag?
            @diag.set_graph(@graph)

    # @param {JointJS.Paper} paper The JointJS Paper used for drawing views.
    set_paper: (paper) ->
        @paper = paper


    set_urlprefix : (str) ->

    to_metamodel: () ->

    switch_to_erd: () ->

    to_erd: () ->

    update_translation: (data) ->

    update_metamodel: (data) ->


    # What to do when the user clicked on a cellView.
    on_cell_clicked: (cellview, event, x, y) ->


    # Clear inputs from relationships form.
    clear_relationship: () ->

    # add_relationship_attr_inverse: (class_id, attribute_id, name)->

    # Update the interface with satisfiable information.
    #
    # @param data {string} is a JSON string with the server response.
    update_satisfiable: (data) ->


    # Update traffic light and models after full reasoning
    #
    # Draft version: only update textarea with data
    #
    # @param data {string} OWLlink string
    update_full_reasoning: (data) ->

    # Set the traffic-light according to the JSON object recived by the server.
    #
    # @param obj {JSON} The JSON object parsed from the recieved data.
    set_trafficlight: (obj) ->

    #
    # Send a POST to the server for checking if the diagram is
    # satisfiable.
    check_satisfiable: () ->


    # Update the translation information on the GUI and show it to the
    # user.
    #
    # Depending on the format selected by the user show it as HTML or
    # inside a textarea tag.
    #
    # @param data {string} The HTML, OWLlink or the translation
    # string.
    # @see CreateClassView#get_translation_format
    update_translation: (data) ->

    # Translate the current model into a formalization.
    #
    # @param strategy {String} The strategy name to use for formalize the model.
    # @param syntax {String} The output sintax format.
    translate_formal: (strategy, syntax) ->

    # Full reasoning on the current model and selecting a reasoner.
    #
    # @param strategy {String} model encoding required for reasoning on.
    # @param syntax {String} reasoning system.
    full_reasoning: (strategy, reasoner) ->

    ##
    # Event handler for translate diagram to OWLlink using Ajax
    # and the api/translate/berardi.php translator URL.
    #
    # @deprecated Use translate_formal() instead.
    translate_owllink: () ->

    change_to_details_page: () ->

    change_to_diagram_page: () ->


    #
    # Hide the left side "Tools" toolbar
    #

    hide_toolbar: () ->

    hide_umldiagram_page: () ->

    show_umldiagram_page: () ->

    hide_eerdiagram_page: () ->

    show_eerdiagram_page: () ->


    # Change the interface into a "new association" state.
    #
    # @param class_id {string} The id of the class that triggered it and thus,
    #   the starting class of the association.
    # @param mult {array} An array of two strings representing the cardinality from and to.
    set_association_state: (class_id, mult, roles, name, w_class, assoc_t) ->

    # Change to the IsA GUI State so the user can select the child for the parent.
    #
    # @param class_id {String} The JointJS::Cell id for the parent class.
    # @param disjoint {Boolean} optional. If the relation has the disjoint constraint.
    # @param covering {Boolean} optional. If the relation has the disjoint constraint.
    set_isa_state: (class_id, disjoint=false, covering=false) ->


    # Change the interface into a "selection" state.
    set_selection_state: () ->

    ##
    # Show the "Insert OWLlink" section.
    show_insert_owllink: () ->


exports.gui.GUIIMPL = GUIIMPL
