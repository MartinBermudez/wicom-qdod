# metamodel.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.metamodel = exports.metamodel ? {}



# Metamodel
#
# @namespace metamodel
class Metamodel

    constructor: () ->
        @serverconn = new ServerConnection (jqXHR, status, text) ->
            exports.gui.gui_instance.widgets.show_error(
                status + ": " + text , jqXHR.responseText)
        
            
    to_erd: (gui_instance) ->        
        umljson = JSON.stringify gui.gui_instance.current_gui.diag.to_json()
        gui.gui_instance.switch_to_gui 'eer'
        @serverconn.request_meta2erd_translation umljson, (data)->
            gui.gui_instance.current_gui.diagadapter.import_jsonstr data
        
    
    
exports.metamodel.Metamodel = Metamodel
exports.metamodel.instance = new Metamodel()
